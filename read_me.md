# Build fungal a species tree from protein fasta files

Developed in January 2019 at MPIPZ by Fantin Mesny

Inspired by https://isugenomics.github.io/bioinformatics-workbook//dataAnalysis/phylogenetics/reconstructing-species-phylogenetic-tree-with-busco-genes-using-maximum-liklihood-method.html

Succesfully used on a 32-fungal genomes dataset



## Dependencies

- Anaconda (or Python 3 with biopython and pandas)
- BUSCO (https://busco.ezlab.org/) and the BUSCO fungal dataset (https://busco.ezlab.org/datasets/fungi_odb9.tar.gz)
- RAxML (https://cme.h-its.org/exelixis/web/software/raxml/index.html)
- ASTRAL-III (https://github.com/smirarab/ASTRAL)


## Input

Input directory needs to contain one folder per organism, containing a proteins.fasta file each.


## Output

The output produced by this protocol is an unrooted species tree. Per-"core protein" alignments and unrooted trees are also available.