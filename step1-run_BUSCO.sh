#!/bin/bash

inp=$1

for f in $inp/*; do
    cd $f/
    for p in *.aa.fasta.gz; do
        gunzip $p -c > proteins.fasta
    done
    run_BUSCO.py -i proteins.fasta -o busco -l busco_db/fungi_odb9 -m prot
done