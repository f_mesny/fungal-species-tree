import os
import argparse
import sys
import pandas as pd
from Bio import SeqIO


def get_params(argv):
	parser = argparse.ArgumentParser(description='parse busco outputs and generate fasta files for alignments and phylogenomics')
	parser.add_argument('-i', '--i', help="input directory, containing dataset", required=True)
	parser.add_argument('-o', '--o', help="output directory", required=True)
	a = parser.parse_args()
	return a


if __name__ == '__main__':
	a = get_params(sys.argv[1:])
	fasta_dic={}
	corresp_dic={}
	for sp in os.listdir(a.i): # /biodata/dep_psl/grp_hacquard/Fantin/jgi42/data/
		path=a.i+'/'+str(sp)
		df=pd.read_csv(path+'/run_busco/full_table_busco.tsv',sep='\t',skiprows=4)
		df=df.set_index('# Busco id')
		corresp_dic[sp]=df[df['Status']=='Complete']['Sequence'].to_dict()
		with open(path+'/proteins.fasta','r') as input_file:
			fasta_dic[sp] = SeqIO.to_dict(SeqIO.parse(input_file, "fasta"))
		for prot in fasta_dic[sp]:
			if fasta_dic[sp][prot][-1]=='*':
				fasta_dic[sp][prot]=fasta_dic[sp][prot][:-1]


	corresp_df=pd.DataFrame(corresp_dic)
	print('INFO:  '+str(len(corresp_df.dropna()))+' BUSCO genes (out of '+str(len(corresp_df))+') are conserved in all genomes')

	dic_allIDs=corresp_df.dropna().T.to_dict()

	for ele in dic_allIDs:
    		fasta=[]
    		for g in dic_allIDs[ele]:
        		fasta_dic[g][dic_allIDs[ele][g]].id=g
        		fasta_dic[g][dic_allIDs[ele][g]].name=g
        		fasta_dic[g][dic_allIDs[ele][g]].description=g
        		fasta.append(fasta_dic[g][dic_allIDs[ele][g]])
		with open(a.o+'/'+ele+'.fasta', 'w+') as handle:
			SeqIO.write(fasta, handle, "fasta")
