#!/bin/bash

inp=$1
outp=$2

cd $inp/
for f in *; do
    mafft --auto $f > $outp/aligned_$f
done
