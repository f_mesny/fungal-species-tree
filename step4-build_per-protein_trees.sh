#!/bin/bash

inp=$1
outp=$2

cd $inp/
for f in *; do
    raxmlHPC-PTHREADS -T 32 -f a -m PROTGAMMAAUTO -s $f -n tree_$f -x 12345 -# 50 -p 12345
    mv *tree_$f $outp/
done
