#!/bin/bash


inp=$1
outp=$2

cat $inp/RAxML_bestTree.tree_aligned_* > $outp/allBestTrees.tree

cd $outp/

java -jar Astral/astral.5.6.3.jar --input allBestTrees.tree --output speciesTree.tree
